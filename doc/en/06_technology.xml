<chapter id="technology">
  <title>Technology</title>

  <sect1 id="metapackages">
  <title>Metapackages</title>

  <sect2 id="defmetapackages">
  <title>Metapackage definition</title>

<para>
A metapackage, as used by Blends, is a Debian package that contains:

<itemizedlist>
  <listitem><para>Dependencies on other Debian packages (essential)</para>
      <variablelist>

        <varlistentry>
          <term>Depends</term>
          <listitem><para>Use &quot;Depends&quot; for packages that are definitely needed
                for all basic stuff of the Blend in question.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>Recommends</term>
          <listitem><para>The packages that are listed as &quot;Recommends&quot; in the
                tasks file should be installed on the machine where the
                metapackage is installed and which are needed to work
                on a specific task.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>Suggests</term>
          <listitem><para>Use &quot;Suggests&quot; for others of lesser importance that
                might be possibly useful, or non-free packages.  When a
                package is not available for the target distribution at
                metapackage build time the &quot;Recommends&quot; is turned into
                a &quot;Suggests&quot; to enable a flawless installation of the
                metapackage.</para>
          </listitem>
        </varlistentry>
      </variablelist>
   </listitem>
   <listitem><para>Menu entries (recommended)</para>
      <itemizedlist>
        <listitem>
          <para>Place these in <filename>/etc/blends/</filename><varname>&lt;blend&gt;</varname>
          <varname>/menu/</varname><varname>&lt;pkg-name&gt;</varname>
          </para>
        </listitem>
        <listitem><para>Maintain these via role based tools</para></listitem>
      </itemizedlist>
   </listitem>
   <listitem>
      <para>Configuration stuff (optional)</para>
      <itemizedlist>
        <listitem><para><orgname>debconf</orgname> questions or pre-seeding</para></listitem>
        <listitem><para><orgname>cfengine</orgname> scripts (or similar see <xref linkend="EnhancingTechnology"/>)</para></listitem>
      </itemizedlist>
   </listitem>
   <listitem><para>Special metapackages:</para>
      <itemizedlist>
         <listitem><para><varname>&lt;blend&gt;</varname>-<package>tasks</package>:
               Contains information for <orgname>tasksel</orgname></para></listitem>
         <listitem><para><varname>&lt;blend&gt;</varname>-<package>config</package>:
               Special configurations, basic stuff for user menus</para></listitem>
      </itemizedlist>
   </listitem>
</itemizedlist>

</para>

<para>
Metapackages are small packages with nearly no contents.  The main
feature of this type of package is its dependencies on other
packages.  The naming of metapackages follows the pattern
<varname>&lt;blend&gt;</varname>-<varname>&lt;task&gt;</varname>
where <varname>&lt;blend&gt;</varname> stands for the short name of a Debian
Pure Blend, e.g. <package>junior</package> for Debian
Jr. or <package>med</package> for Debian Med,
and <varname>&lt;task&gt;</varname> means the certain task inside the Blend,
e.g. puzzle or bio.
</para>

<para>

Examples:
<variablelist>

<varlistentry>
 <term><package>junior-puzzle</package></term>
  <listitem><para>Debian Jr. Puzzles</para></listitem>
</varlistentry>

<varlistentry>
 <term><package>education-tasks</package></term>
  <listitem><para>Tasksel files for SkoleLinux systems</para></listitem>
</varlistentry>

<varlistentry>
 <term><package>med-bio</package></term>
  <listitem><para>Debian Med micro-biology packages</para></listitem>
</varlistentry>

</variablelist>
</para>

</sect2>
  <sect2 id="collection">
  <title>Collection of specific software</title>

<para>
When using metapackages, no research for available software inside Debian is
necessary.  It would not be acceptable for normal users to have to browse the
descriptions of the whole list of the 20000 packages in Debian to find
everything they need.  So, metapackages are an easy method to help users to
find the packages that are interesting for their work quickly.
</para>
<para>
If the author of a metapackage includes several packages with similar
functionality, an easy comparison between software covering the same task is
possible.
</para>
<!-- This is not true any more since we turn Depends into Recommends
     to explicitely *enable* users to remove certain packages.

Moreover, the installation of a metapackage ensures that no package
that is necessary for the intended task can be removed without
explicit notice that also the metapackage has to be removed.  This
helps non specialist administrators to keep the installation fit for
the specialized users.
 -->
<para>
By defining conflicts with some other packages inside the metapackage,
it is possible to ensure that a package that might conflict for some
reasons for the intended task can not be installed at the same time as
the metapackage is installed.
</para>
<para>
All in all, metapackages enable an easy installation from scratch, and
keep the effort required for administration low.
</para>

</sect2>
  <sect2 id="categorisation">
  <title>Packages showing up in more than one metapackage</title>

<para>
This seems to be an FAQ: If a package <package>A</package> is in the list of
dependencies of metapackage <package>m</package> is it allowed or reasonable to
add it to the list of dependencies of metapackage <package>n</package>?
</para>
<para>
The answer is: Why not?
</para>
<para>
The &quot;overlap&quot; is no problem because we do not want to build
an exclusive categorisation which might be hard  to understand for our
users.  Metapackages are like &quot;normal&quot; packages: Nobody
would assume that because package <package>x</package> depends from package
<package>libc</package> no other package is allowed to add <package>libc</package> to its
depends.  So why not adding a dependency to more than one metapackage if
it is just useful for a certain task?
</para>

<para>
The important thing is to support our users. A specific user wants to
solve a certain task (and thus installs a certain metapackage).  The
question whether some Dependencies are also mentioned in a different
metapackage is completely useless for this task.  So in fact we do
<emphasis>not</emphasis> build a categorisation tree but build pools of useful
software for certain tasks which can definitely have overlaps.
</para>

<para>
To give a certain example which was asked by a member of Debian
Multimedia team: A user who is seeking for his optimal sound player is
not served best if we &quot;hide&quot; an application from his view by
including it into sound recorders exclusively.  While chances might be
good that a sound recorder is not as lightweight as a pure player the
user will find out this quickly if he is looking for only a
lightweight player - but perhaps he becomes happy later about the
&quot;added value&quot; of his favourite player if it also is able to record
sound.
</para>

   </sect2>

   <sect2 id="configuration">
   <title>Adapted configuration inside metapackages</title>

<para>
Besides the simplification of installing relevant packages by
dependencies inside metapackages, these packages might contain special
configuration for the intended task.  This might either be
accomplished by pre-seeding <orgname>debconf</orgname> questions, or by
modifying configuration files in a <package>postinst</package> script.  It
has to be ensured that no changes that have been done manually by the
administrator will be changed by this procedure.  So to speak, the
<package>postinst</package> script takes over the role of a local
administrator.
<!-- FIXME:
 An improved version of the pre-seeding scripts are included in newer
 debconf versions.  I just replaced the debian-edu scripts with the
 scripts from debconf.  Look at debconf-set-selections and
 debconf-get-selections.
 -->
</para>

   </sect2>

   <sect2 id="documentation">
   <title>Documentation packages</title>

<para>
A "traditional" weakness of Free Software projects is missing
documentation.  To fix this, Debian Pure Blends try to provide
relevant documentation to help users to solve their problems.  This
can be done by building <package>*-doc</package> packages of existing
documentation, and by writing extra documentation, like manpages, etc.
By supplying documentation, Debian Pure Blends fulfil their role in
addressing the needs of specialised users, who have a great need for
good documentation in their native language.
</para>
<para>
Thus, translation is a very important thing to make programs more
useful for the target user group.  Debian has established
a <ulink url="http://ddtp.debian.net/">Debian Description
Translation Project</ulink>, which has the goal to translate package
descriptions.  There is a good chance this system could also be used
for other types of documentation, which might be a great help for
Debian Pure Blends.
</para>

   </sect2>
   </sect1>

   <sect1 id="mp_handling">
   <title>Handling of metapackages</title>

<para>
<!-- [BA] I think we need to qualify what 'handle' means, because there
     certainly are other tools that 'handle' them (e.g. synaptic) if that term
     is taken in its broadest sense.
     [AT] ACK, here that we have to define 'handle'.  The term
     'nicely' was intended to be the keyword because metapackages are
     currently handled as any other package which is fine, but no help
     for users who do not know anything about metapackages.  We need
     special tools to gain all profits we could have.  Could you set
     this into good English??? 
  -->
In short, there are no special tools available to handle metapackages
nicely. But there are some tricks that might help, for the moment.
</para>

   <sect2 id="cmdline">
   <title>Command line tools</title>

<para>
<variablelist>

<varlistentry>
  <term><orgname>apt-cache</orgname></term>
   <listitem><para>
    The program <orgname>apt-cache</orgname> is useful to search for
    relevant keywords in package descriptions.  With it, you could search
    for a certain keyword connected to your topic (for instance
   &quot;<package>med</package>&quot;) and combine it reasonably with <package>grep</package>:</para>
    <informalexample>
      <programlisting>
~> apt-cache search med | grep '^med-'
med-bio - Debian Med micro-biology packages
med-bio-dev - Debian Med micro-biology development packages
med-doc - Debian Med documentation packages
med-imaging - Debian Med imaging packages
med-imaging-dev - Debian Med packages for medical image develop...
med-tools - Debian Med several tools
med-common - Debian Med Project common package
med-cms - Debian Med content management systems
</programlisting>
    </informalexample>
  <para>
    This is <emphasis>not really straightforward</emphasis>, and
   absolutely unacceptable for end users.
  </para>

   </listitem>
</varlistentry>

<varlistentry>
  <term><package>grep-dctrl</package></term>
   <listitem><para>
    The program <package>grep-dctrl</package> is a grep for Debian package
    information, which is helpful for extracting specific package details
    matching certain patterns:</para>
    <informalexample>
      <programlisting>
~> grep-dctrl ': med-' /var/lib/dpkg/available | \
   grep -v '^[SIMAVF]' | \
   grep -v '^Pri'
Package: med-imaging
Depends: paul, ctsim, ctn, minc-tools, medcon, xmedcon, med-common
Description: Debian Med imaging packages

Package: med-bio
Depends: bioperl, blast2, bugsx, fastdnaml, fastlink, garlic...
Description: Debian Med micro-biology packages

Package: med-common
Depends: adduser, debconf (>= 0.5), menu
Description: Debian Med Project common package

Package: med-tools
Depends: mencal, med-common
Description: Debian Med several tools

Package: med-doc
Depends: doc-linux-html | doc-linux-text, resmed-doc, med-co...
Description: Debian Med documentation packages

Package: med-cms
Depends: zope-zms
Description: Debian Med content management systems

Package: med-imaging-dev
Depends: libgtkimreg-dev, ctn-dev, libminc0-dev, libmdc2-dev...
Description: Debian Med packages for medical image development

Package: med-bio-contrib
Depends: clustalw | clustalw-mpi, clustalx, molphy, phylip, ...
Description: Debian Med micro-biology packages (contrib and ...
</programlisting>
    </informalexample>
    <para>
      This is, like the <package>apt-cache</package> example, <emphasis>also
      a bit cryptic</emphasis>, and again is not acceptable for end users.
    </para>
   </listitem>
</varlistentry>

<varlistentry>
  <term><package>auto-apt</package></term>
   <listitem><para>
    The program <package>auto-apt</package> is really cool if you are
    running a computer that was installed from scratch in a hurry, and
    are sitting at a tradeshow booth preparing to do a demo.  If you had
    no time to figure out which packages you needed for the demo were missing
    so you could install all of them in advance, you could use
    <package>auto-apt</package> in the following manner to guarantee that you
    have all of the files or programs you need:</para>
    <informalexample>
      <programlisting>
~> sudo auto-apt update
put: 880730 files,  1074158 entries
put: 903018 files,  1101981 entries
~> auto-apt -x -y run
Entering auto-apt mode: /bin/bash
Exit the command to leave auto-apt mode.
bash-2.05b$ less /usr/share/doc/med-bio/copyright
Reading Package Lists... Done
Building Dependency Tree... Done
The following extra packages will be installed:
  bugsx fastlink readseq 
The following NEW packages will be installed:
  bugsx fastlink med-bio readseq 
0 packages upgraded, 4 newly installed, 0 to remove and 183 ...
Need to get 0B/1263kB of archives. After unpacking 2008kB wi...
Reading changelogs... Done
Selecting previously deselected package bugsx.
(Reading database ... 133094 files and directories currently...
Unpacking bugsx (from .../b/bugsx/bugsx_1.08-6_i386.deb) ...
Selecting previously deselected package fastlink.
Unpacking fastlink (from .../fastlink_4.1P-fix81-2_i386.deb) ...
Selecting previously deselected package med-bio.
Unpacking med-bio (from .../med-bio_0.4-1_all.deb) ...
Setting up bugsx (1.08-6) ...

Setting up fastlink (4.1P-fix81-2) ...

Setting up med-bio (0.4-1) ...

localepurge: checking for new locale files ...
localepurge: processing locale files ...
localepurge: processing man pages ...
This package is Copyright 2002 by Andreas Tille &lt;tille@debian.org&gt;

This software is licensed under the GPL.

On Debian systems, the GPL can be found at /usr/share/common-...
/usr/share/doc/med-bio/copyright
</programlisting>
    </informalexample>

    <para>
      Just do your normal business - in the above example, <filename>less
      /usr/share/doc/med-bio/copyright</filename> - and if the necessary
      package is not yet installed, <package>auto-apt</package> will care for
      the installation and proceed with your command.  While this is
      really cool, this is <emphasis>not really intended for a production
      machine</emphasis>.
    </para>

   </listitem>
</varlistentry>

</variablelist>

The short conclusion here is: <emphasis>There are no sophisticated tools
that might be helpful to handle metapackages as they are used in
Debian Pure Blends - just some hacks using the powerful tools inside
Debian.</emphasis>
</para>

   </sect2>

   <sect2 id="text_ui">
   <title>Text user interfaces</title>
<para>

<variablelist>

<varlistentry>
  <term><orgname>tasksel</orgname></term>
   <listitem>
    <para>
    The Debian task installer <package>Tasksel</package> is the first
    interface for package selection that is presented to the user when
    installing a new computer.  The <package>End-user</package> section should
    contain an entry for each Debian Pure Blend.  
    Unfortunately, there are some issues that prevent Blends
    from being included in the <package>tasksel</package> list,
    because the dependencies of this task can affect what appears on
    the first installation CD.  This problem would be even greater if
    all Blends were added, and so a different solution has
    to be found here. (See <ulink url="http://bugs.debian.org/186085">#186085</ulink>.)
      In principle, <package>tasksel</package> is a good
    tool for easy installation of Blends.
    </para>
    <para>
    As a workaround for this problem the <package>blends-dev</package>
    framework creates a package <package>BLEND-tasks</package> which contains
    a <package>tasksel</package> control file.  If you install this package
    all tasks of the Blend will be added to the default list of tasks
    inside <package>tasksel</package>.  So a solution for Blend specific
    installation media might be to just remove the default tasksel
    list and provide the Blends own tasks exclusively.
    </para>
   </listitem>
</varlistentry>

<varlistentry>
  <term><package>aptitude</package></term>
   <listitem><para>
    This is a better replacement for <package>dselect</package>, and has
    some useful support for searching for and grouping of packages.
    While this is not bad, it was not intended for the purpose of
    handling Debian Pure Blends, and thus there could be some better
    support to handle metapackages more cleverly.</para>
   </listitem>
</varlistentry>

</variablelist>

Short conclusion: <emphasis>There is a good chance metapackages could be
handled nicely by the text based Debian package administration tools,
but this is not yet implemented.</emphasis>
</para>

   </sect2>
         
   <sect2 id="gui">
   <title>Graphical user interfaces</title>
<para>
Debian <emphasis>Woody</emphasis> does not contain a really nice graphical
user interface for the Debian package management system.  But the
efforts to support users with an easy to use tool have increased, and
so there there will be some usable options in Sarge.

<variablelist>

<varlistentry>
  <term><package>gnome-apt</package></term>
   <listitem><para>This is the native GNOME flavour of graphical user interfaces
         to apt.  It has a nice <package>Search</package> feature that can be
         found in the <package>Package</package> menu section.  If for instance
         the packages of the Debian Jr. project come into the focus of
         interest a search for &quot;<package>junior-*</package>&quot; will show
         up all related packages including their descriptions.  This
         will give a reasonable overview about metapackages of the
         project.</para>
   </listitem>
</varlistentry>

<varlistentry>
  <term><package>synaptic</package></term>
   <listitem><para>Even more sophisticated and perhaps the best choice for users
         of Debian Pure Blends.  <package>Synaptic</package> has a nice
         filter feature, which makes it a great tool here.
         Moreover <package>synaptic</package> is currently the only user
         interface that supports Debian Package Tags
         (see <xref linkend="debtags"/> ).</para>
   </listitem>
</varlistentry>

<varlistentry>
  <term><package>kpackage</package></term>
   <listitem><para>This is the user interface of choice for KDE lovers.
         Regarding its features (with exception of Debian Package Tags)
	 it is similar to both above.</para>
   </listitem>
</varlistentry>

</variablelist>

Short conclusion: <emphasis>As well as the text based user interfaces
these tools are quite usable but need enhancements to be regarded as
powerful tools for Debian Pure Blends.</emphasis>
</para>
   </sect2>
         
   <sect2 id="web_if">
   <title>Web interfaces</title>

<para>

<variablelist>

<varlistentry>
  <term>Tasks pages</term>
   <listitem><para>The tasks pages probably provide the best overview about
         the actual work which is done in a Debian Pure Blend.  These
         pages are automatically generated by reading the tasks files
         (see <xref linkend="debian_control"/> ) and verifying the existence
         of the packages that are mentioned as dependencies.  On the
         resulting web page the packages are listed with some meta
         information and the description of the package.  As user
         oriented pages they are translated into more than 10
         languages while translated means, the navigation text of the
         page generating code is using <package>gettext</package> which
         enables translation (the work is not yet completely done for
         all languages) but even more importantly the descriptions of
         the packages are translated as well by using the information
         from
         <ulink url="http://ddtp.debian.net/">Debian Description Translation Project</ulink>.
         </para>
         <para>These tasks pages are available via
	  <informalexample>
      <programlisting>
            http://blends.debian.org/BLEND/tasks
          </programlisting>
          </informalexample>
	  where <package>BLEND</package> has to be replaced by the name of the
	  Blend.  Currently these pages are available for the Blends:
	  <informalexample>
      <programlisting>
	    accessibility, edu, gis, junior, lex, med, science, debichem
    </programlisting>
    </informalexample>
	  In short: If you want to know more about a specific Blend
	  go to its task page and have a look what is listed there.
         </para>
   </listitem>
</varlistentry>

<varlistentry>
  <term>Bugs pages</term>
   <listitem><para>The more developer oriented bugs pages try to match the
         scope of the tasks pages mentioned above but there is no
         description of the packages given but rather the bugs that
         are reported in the Debian Bug Tracking System (BTS) are
         listed there.  This is a quite valuable source of information
         if somebody is interested in increasing the quality of a
         Blend: Fixing bugs is always welcome and listing all relevant
         bugs at a single place is a nice way to detect problems
         quickly.
         </para>
         <para>These bugs pages are available via
	  <informalexample>
      <programlisting>
            http://blends.debian.org/BLEND/bugs
          </programlisting>
          </informalexample>
	  where <package>BLEND</package> has to be replaced by the name of the
	  Blend.  Currently these pages are available for the Blends:
	  <informalexample>
      <programlisting>
	    accessibility, edu, gis, junior, lex, med, science, debichem
    </programlisting>
          </informalexample>
	  In short: If you want to help enhancing the quality of a
	  specific Blend go to its bug page and start working on the
	  bugs listed there.
         </para>
   </listitem>
</varlistentry>

<varlistentry>
  <term><ulink url="http://packages.debian.org/">Web search</ulink></term>
   <listitem><para>Debian has a web interface that can be used to search for certain substrings
         in package names.  For instance if you are searching the meta
         packages of Debian Med you could point your favourite Browser
         to</para>
         <para>
<remark>FIXME: &amp; is sometimes broken!!!
<ulink url="http://packages.debian.org/cgi-bin/search_packages.pl?keywords=med-&amp;subword=1"/>
                                                                            ^^^^^
</remark>
<ulink url="http://packages.debian.org/cgi-bin/search_packages.pl?keywords=med-&amp;subword=1"/>
         </para>
         <para>
         As a result you will get a list of all Debian Med packages.</para>
   </listitem>
</varlistentry>

<varlistentry>
  <term><ulink url="http://qa.debian.org/developer.php">Package Tracking System</ulink></term>
   <listitem><para>
         The Package Tracking System is a really great tool that
         provides essential information about packages.  Most Debian
         Pure Blends are using a mailing list address as Maintainer of
         their key packages which includes the metapackages.  This so
         called team maintenance of packages is on one hand very handy
         from a developers point of view on the other hand it enables
         using the Package Tracking System to get a quick overview:
          </para>
         <variablelist>
          <varlistentry>
              <term>Debian Jr:</term>
               <listitem><para>
                <ulink url="http://qa.debian.org/developer.php?login=debian-jr@lists.debian.org"/>
                  </para>
               </listitem>
          </varlistentry>

          <varlistentry>
              <term>Debian Med:</term>
               <listitem> <para>
                <ulink url="http://qa.debian.org/developer.php?login=debian-med-packaging@lists.alioth.debian.org"/>
                  </para>
               </listitem>
          </varlistentry>

          <varlistentry>
              <term>Debian Edu:</term>
               <listitem><para>
                <ulink url="http://qa.debian.org/developer.php?login=debian-edu@lists.debian.org"/>
                </para>
               </listitem>
          </varlistentry>

          <varlistentry>
              <term>Debian Science:</term>
               <listitem><para>
                <ulink url="http://qa.debian.org/developer.php?login=debian-science-maintainers@lists.alioth.debian.org"/>
                  </para>
               </listitem>
          </varlistentry>

         </variablelist>
         <para>
	 Hint: If you append the option <package>&amp;ordering=3</package> you
	 might get some sectioning of this page according to the
	 metapackage categories.  This result is approached by a tool
	 which subscribes all dependent packages to the group
	 maintenance address and adds a section according to a
	 metapackage name.
	 </para>
         <para>
         The other way to use the Package Tracking System is to search
         for packages starting with a certain letter:
         <variablelist>
          <varlistentry>
              <term>Debian Jr:</term>
               <listitem><para>
                <ulink url="http://packages.qa.debian.org/j"/></para>
               </listitem>
          </varlistentry>

          <varlistentry>
              <term>Debian Med:</term>
               <listitem><para>
                <ulink url="http://packages.qa.debian.org/m"/></para>
               </listitem>
          </varlistentry>

         </variablelist>
         But the list that is obtained by this method is much larger
         than it would be useful for a good overview.
         </para>
  </listitem>
</varlistentry>

<varlistentry>
  <term><package>list-junior.sh</package></term>
   <listitem><para>The package <package>junior-doc</package> contains a script
         <filename>/usr/share/doc/junior-doc/examples/scripts/list-junior.sh</filename>
         that checks for the installed packages of a Blend and builds
         a simple web page describing these packages.  (The BTS
         contains a patch to let this script work also for other
         Blends.)</para>
   </listitem>
</varlistentry>

</variablelist>

Short conclusion: <emphasis>The Debian Pure Blends provide some nice web
  tools for a whole set of packages for a certain working field that
  provide a better overview than the usual Debian tools that are
  basically dealing with single packages..</emphasis>
</para>
   </sect2>
         
   <sect2 id="future_handling">
   <title>Future handling of metapackages</title>

<para>
Obviously there are no nifty tools as you might know them from Debian
available yet.  The user interfaces for <package>apt-get</package> have to be
enhanced drastically to make them easy enough to make them useful in
the hands of an end user.  This might implicitly mean that we need
some additional control fields in <package>dpkg</package> to implement
reasonable functionality.  The following items are target of future
development:
<itemizedlist>
    <listitem><para>Searching for existing metapackages</para></listitem>
    <listitem><para>Overview about dependencies of these metapackages</para></listitem> 
    <listitem><para>Enhancing tools like <package>aptitude</package>,
          <package>synaptic</package>, etc.</para></listitem>
    <listitem><para>Special <package>tasksel</package> section</para></listitem> 
    <listitem><para>Web tools that keep metapackage information up to
          date</para></listitem>
</itemizedlist>

</para>

<para>
Furthermore it is necessary to find a set of keywords for each Debian
Pure Blend and write a tool to search these keywords comfortable.  The
best way to accomplish this might be to make use of Debian Package
Tags, which is a quite promising technique.
</para>

<para>
Tools that grep the apt cache directly for metapackages have to be
written or rather the available tools for this should be patched for
this actual functionality.
</para>
   </sect2>
   </sect1>

  <sect1 id="userroles">
  <title>User roles</title>

<para>
As stated above specialists have only interest in a subset of the
available software on the system they are using.  In an ideal world, this
would be the only software that is presented in the menu.  This would
allow the user to concentrate on his real world tasks instead of
browsing large menu trees with entries he does not understand.
</para>

<para>
To accomplish this, a technique has to be implemented that allows to
define a set of users who get a task-specific menu while getting rid
of the part of software they are not interested in.  Moreover this has
to be implemented for certain groups of users of one Blend, which are
called "roles".  There are several techniques available to manage user
roles.  Currently in the field of Debian Pure Blends a UNIX group
based role system is implemented.  This means, that a user who belongs
to a certain group of a Blend is mentioned in
the <filename>/etc/group</filename> file in the appropriate group and gets a
special user menu that is provided for exactly this group.
</para>

<para>
Strictly speaking it is not the best solution to conflate a
configuration mechanism, which users see with menus, with access
control, i.e. unix groups.  It might be confusing, and wastes the limited
number of groups to which a user can belong.  On the other hand this
is a solution that works for the moment, and has no real negative
impact on the general use of the system.  The benefit of using unix
groups is that there is a defined set of tools provided to handle user
groups.  This makes life much easier; there is no
<emphasis>practical</emphasis> limit to the number of groups to which a user may
belong for the existing Debian Pure Blends at this time.
</para>
<para>
In the long run, this role system might even be enhanced to certain
"<emphasis>levels</emphasis>" a user can have and here the UNIX groups approach
will definitely fail and has to be replaced by other mechanisms.  This
will include the possibility to enable the user adjust his own level
("novice", "intermediate", "expert") while only the administrator is
able to access the UNIX groups. On the other hand such kind of user
level maintenance is not only a topic for Debian Pure Blends but might
be interesting for Debian in general.
</para>

<para>
Another point that speaks against using UNIX groups for role
administration is the fact that local administrators are not in all
cases competent enough to understand the UNIX role concept as a
security feature and thus a real role concept including tools to
maintain roles are needed in the future.
</para>

<para>
The handling of the user menus according to the groups is implemented
in a flexible plugin system and other ways of handling groups
(i.e. LDAP) should be easy to implement. 
</para>

  <sect2 id="menu_tools">
  <title>User menu tools</title>

    <sect3 id="user-menus">
    <title>Using the Debian menu system</title>
<para>
The Debian menu system cares for menu updates after each package
installation.  To enable compliance with the <emphasis>role</emphasis> based menu
approach it is necessary to rebuild the user menu after each package
installation or after adding new users to the intended role.  This can
be done by using the 
<citerefentry><refentrytitle>blend-update-menus</refentrytitle>
  <manvolnum>8</manvolnum></citerefentry> (see
<xref linkend="blend-update-menus"/>) script from
<package>blends-common</package>.  It has to be said that using
<orgname>blend-update-menus</orgname> is not enough to change the menu of a
user.  To accomplish this a call of the general
<orgname>update-menu</orgname> script for every single user of a Blend is
necessary if this is not done by the
<filename>postinst</filename> script of a metapackage.  This can easily been
done if the configuration file of a Debian Pure Blend
<filename>/etc/blends/</filename><varname>&lt;blend&gt;</varname>/<varname>&lt;blend&gt;</varname><filename>.conf</filename> contains the
line
<informalexample>
<programlisting>
    UPDATEUSERMENU=yes
  </programlisting>
</informalexample>

</para>
<para>
It is strongly suggested to use the
package <package>blends-dev</package> to build metapackages of a
Debian Pure Blend that will move all necessary files right into place
if there exists a
<filename>menu</filename> directory with the menu entries.  Note, that the users
<filename>${HOME}/.menu</filename> directory remains untouched.
</para>
     </sect3>

     <sect3 id="user-debconf">
     <title>Managing Debian Pure Blend users with <orgname>debconf</orgname></title>
 
<para>
Using <package>blends-dev</package> it is very easy to build a
<varname>blend</varname><package>-config</package> package that contains
<orgname>debconf</orgname> scripts to configure system users who should
belong to the group of users of the Debian Pure Blend <varname>blend</varname>.
For example see the <package>med-common</package> package.

    <informalexample>
      <programlisting>
~> dpkg-reconfigure med-common

Configuring med-common
----------------------

Here is a list of all normal users of the system.  Now you can select those users who 
should get a Debian Med user menu.

  1. auser (normal user A)        6. fmeduser (med user F)
  2. bmeduser (med user B)        7. glexuser (lex user G)
  3. cjruser (jr user C)          8. hmeduser (med user H)
  4. djruser (jr user D)          9. iadmin (administrator I)
  5. eadmin (administrator E)     10. juser (normal user J)

(Enter the items you want to select, separated by spaces.)

:-! Please specify the Debian Med users! 2 8
</programlisting>
    </informalexample>
This example shows the situation when you
<package>dpkg-reconfigure</package> <package>med-common</package> if
<varname>med user B</varname> and <varname>med user H</varname> were defined as users
of Debian Med previously and <varname>med user F</varname> should be added to
the group of medical staff.  (For sure it is more convenient to use the
more comfortable interfaces to <package>debconf</package> but the used
SGML DTD <ulink url="http://bugs.debian.org/140684">does not yet
    support screen shots</ulink>.)
</para>
    </sect3>
   </sect2>
  </sect1>
         
   <sect1 id="devtools">
   <title>Development tools</title>

<para>
Building a metapackage is more or less equal for each meta
package. This was the reason to build a common source package
<package>blend</package> that builds into two binary packages
<variablelist>

<varlistentry>
  <term><package>blends-dev</package></term>
   <listitem><para>Helpful tools to build metapackages from a set of template
         files.  These tools are interesting for people who want to
	 build metapackages in the style Debian Edu and Debian Med
	 are currently doing this.  The purpose of this package is to
	 make maintenance of metapackages as easy as possible.</para>
	 <para>This package is described in detail in appendix <xref
         linkend="blends-dev"/>.</para>
   </listitem>
</varlistentry>

<varlistentry>
  <term><package>blends-common</package></term>
   <listitem><para>This package provides some files that are common to meta
         packages of Debian Pure Blends especially those that were
         built using the tools of the package
	 <package>blends-dev</package>. It introduces a method to handle
         system users in a group named according to the name of the
         Blend.  The user menu approach is explained in detail
         in <xref linkend="userroles"/>.</para>
	 <para>This package is described in detail in appendix <xref
         linkend="blends-common"/>.</para>
   </listitem>
</varlistentry>

</variablelist>

The usage of the tools that are contained in these packages are
described now in detail.
</para>

</sect1>
   <sect1 id="namespace">
   <title>Dealing with name space pollution</title>

<para>
Due to the fact that Blends might deal with quite specialised software
the user base is often quite small.  In such use cases it happens that
some programs are using names that are used by other more frequently
used tools.  For instance the Debian Med program <package>plink</package>
has the same name as the program <filename>plink</filename> that belongs
to the ssh clone <package>putty</package>.  According to the Debian policy
both packages, <package>putty</package> and <package>plink</package> need
to be co-installable.  Thus one tool has to be renamed.
</para>
<para>
Name space conflicts in Debian are usually solved by the principle that
whoever comes first has taken the name if there is no better agreement.
However, it makes sense to keep the original name chosen by upstream
for the more frequently used program - and here it might be that the
Blends packager steps back.  There might be a chance to discuss with
upstream about a better name but there is no guarantee that this will be
successfully and thus there is another option for the Blends developer
to provide the original upstream name to the Blends users by circumventing
the file name conflict - at least for users of the <filename>bash</filename>
shell.
</para>
<para>
Since <package>blends-dev >= 0.6.92.3</package> in the 
<varname>blend</varname><package>-config</package> a <filename>bash</filename>
init script is provided in
</para>
    <informalexample>
      <programlisting>
  /etc/profile.d/<varname>&lt;BLEND-name&gt;</varname>.sh
      </programlisting>
    </informalexample>
<para>
This script is executed in login shells and checks for two things:
</para>
<para>
<orderedlist>
  <listitem><para>
    Does the path <filename>/usr/lib/<varname>&lt;BLEND-name&gt;</varname>/bin</filename>
    exist (since a package installs files there)?
  </para></listitem>
  <listitem><para>
    Has the user starting the <filename>bash</filename> instance created a file
    <filename>$HOME/.blends</filename> and is there a line featuring the name
    of the Blend (in the example above "debian-med")
  </para></listitem>
</orderedlist>
</para>
<para>
If both conditions are fulfilled the PATH gets prepended by
<filename>/usr/lib/<varname>&lt;BLEND-name&gt;</varname>/bin</filename>
and thus the tools residing in this directory were found first.
</para>
<para>
Moreover also MANPATH is prepended by
<filename>/usr/lib/<varname>&lt;BLEND-name&gt;</varname>/share/man</filename>
to enable providing proper manpages to the binaries in the Blends private
PATH.  Otherwise it might happen that a user might see a manpage of the
executable in <filename>/usr/bin</filename> which is not the first in the
search PATH any more.  The location is a topic of further discussion since
manpages under <filename>/usr/lib</filename> are in conflict with FHS.
</para>
   </sect1>

</chapter>
